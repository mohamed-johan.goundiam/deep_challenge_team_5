# coding: utf-8

# External imports
import torch

from .cnn_models import *

# Local imports
from .handlers import *


def get_handler_name(cfg):
    return eval(f"{cfg['class']}")


def build_model(cfg, input_size, num_classes):
    return eval(f"{cfg['class']}(cfg, input_size, num_classes)")
