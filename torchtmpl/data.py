# coding: utf-8

# Standard imports
import logging
import random

import matplotlib.pyplot as plt
import numpy as np

# External imports
import torch
import torch.utils.data
from GCE.data_loading.pytorch_dataset import GeoLifeCLEF2022Dataset
from torchvision import transforms


def create_transforms(cfg):
    transforms_fns = [
        (eval(transf), {kwarg: eval(v) for kwarg, v in kwargs.items()})
        for transf_dict in cfg
        for transf, kwargs in transf_dict.items()
    ]
    return transforms.Compose([t(**kwargs) for t, kwargs in transforms_fns])


class StackPatches(object):
    """
        Transforms the list of patches into a single array

    Args:
        cout (tuple, optional): Desired channels in the output array. If None, all the channels are stacked.
    """

    def __init__(self, cout=None):
        assert isinstance(cout, tuple) or cout is None
        self.cout = cout

    def __call__(self, sample: list) -> np.ndarray:
        arr = np.dstack(sample)

        if self.cout is None:
            return arr.astype(float)
        return np.delete(
            arr, [ax for ax in range(arr.shape[-1]) if ax not in self.cout], axis=-1
        )


class TestDataSet(GeoLifeCLEF2022Dataset):
    """
    We need this Test_DataSet to generate (Obs_Id, Test_features) items
    """

    def __getitem__(self, index):
        return (
            self.observation_ids[index],
            super(TestDataSet, self).__getitem__(index)[0],
        )


def show_image(X):
    num_c = X.shape[0]
    plt.figure()
    plt.imshow(X[0] if num_c == 1 else X.permute(1, 2, 0))
    plt.show()


def get_dataloaders(data_config, use_cuda):
    valid_ratio = data_config["valid_ratio"]
    batch_size = data_config["batch_size"]
    num_workers = data_config["num_workers"]
    transforms_clf = data_config["transforms"]

    logging.info("  - Dataset creation")

    geolife_dataset = GeoLifeCLEF2022Dataset(
        data_config["path"],
        transform=create_transforms(transforms_clf),
        **data_config["dataset_kwargs"],
    )

    logging.info(f"  - I loaded {len(geolife_dataset)} samples")

    indices = list(range(len(geolife_dataset)))
    random.shuffle(indices)
    num_valid = int(valid_ratio * len(geolife_dataset))
    train_indices = indices[num_valid:]
    valid_indices = indices[:num_valid]

    train_dataset = torch.utils.data.Subset(geolife_dataset, train_indices)
    valid_dataset = torch.utils.data.Subset(geolife_dataset, valid_indices)

    # Build the dataloaders
    train_loader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
        pin_memory=use_cuda,
    )

    valid_loader = torch.utils.data.DataLoader(
        valid_dataset,
        batch_size=batch_size,
        shuffle=False,
        num_workers=num_workers,
        pin_memory=use_cuda,
    )

    num_classes = len(geolife_dataset.categories)
    input_size = tuple(geolife_dataset[0][0].shape)

    return train_loader, valid_loader, input_size, num_classes


def get_dataloaders_submission(config, use_cuda):
    test_dataset = TestDataSet(
        config["path"],
        transform=create_transforms(config["transforms"]),
        **config["dataset_kwargs"],
    )

    num_classes = len(test_dataset.categories)
    input_size = tuple(test_dataset[0][1].shape)

    test_loader = torch.utils.data.DataLoader(
        test_dataset,
        batch_size=config["batch_size"],
        shuffle=False,
        num_workers=config["num_workers"],
        pin_memory=use_cuda,
    )

    return test_loader, input_size, num_classes
